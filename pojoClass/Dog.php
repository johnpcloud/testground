<?php
namespace pojoClass;

class Dog
{
    public $dogName;
    public $legs;

    /**
     * @return mixed
     */
    public function getDogName()
    {
        return $this->dogName;
    }

    /**
     * @param mixed $dogName
     * @return Dog
     */
    public function setDogName($dogName)
    {
        $this->dogName = $dogName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLegs()
    {
        return $this->legs;
    }

    /**
     * @param mixed $legs
     * @return Dog
     */
    public function setLegs($legs)
    {
        $this->legs = $legs;
        return $this;
    }

}