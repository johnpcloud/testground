<?php
namespace pojoClass;

class SchemaConverter
{
    public function pojoToJson($classObject)
    {
        $camelCaseKeys = get_object_vars($classObject);

        $newKeys = [];
        $newValue = [];
        foreach ($camelCaseKeys as $key => $value) {

            $snakeCaseKeys = self::toSnakeCase($key) ;

            $funcName = 'get'. ucfirst($key);

            array_push($newKeys, $snakeCaseKeys);
            array_push($newValue, $classObject->$funcName());
        }

        $response = array_filter( array_combine($newKeys, $newValue) );

        return json_encode($response);
    }

    public function jsonToPojo($json, $className)
    {

        $object = new $className;

        $decoded = json_decode($json);

        foreach ($decoded as $key=>$value) {

            $camelCase = SchemaConverter::toCamelCase($key);

            $funcName = 'set'. ucfirst($camelCase);

            $object->$funcName($value);
        }

        return $object;
    }

    private function toSnakeCase($word) : string
    {
        return strtolower( preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $word) );
    }

    private function toCamelCase($word) : string
    {

        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $word))));
    }



}