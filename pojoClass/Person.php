<?php
namespace pojoClass;

class Person
{
    public $firstName;
    public $lastName;
    public $age;
    public $homeAddressSomething;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     * @return Person
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeAddressSomething()
    {
        return $this->homeAddressSomething;
    }

    /**
     * @param mixed $homeAddressSomething
     * @return Person
     */
    public function setHomeAddressSomething($homeAddressSomething)
    {
        $this->homeAddressSomething = $homeAddressSomething;
        return $this;
    }

}

