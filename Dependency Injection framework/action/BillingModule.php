<?php
namespace Action;

use Ray\Di\AbstractModule;
use Ray\Di\Injector;

class BillingModule extends AbstractModule
{

    /**
     * @inheritDoc
     */
    protected function configure()
    {
//        $this->bind(ProcessorInterface::class)->to(PaypalProcessor::class);
//        $this->bind(LoggerInterface::class)->to(DatabaseLogger::class);

//        $this->bind(TransactionLogInterface::class)->toProvider(DatabaseTransactionLogProvider::class);
//        $this->bind(TransactionLogInterface::class)->to(DatabaseTransactionLogProvider::class);

        return "this";
    }
}


$injector = new Injector(new BillingModule);
$billingService = $injector->getInstance(BillingService::class);


print_r( $billingService );