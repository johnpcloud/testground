<?php
namespace Action;

use Ray\Di\ProviderInterface ;

class DatabaseTransactionLogProvider implements ProviderInterface
{

    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function get()
    {
        $transactionLog = new DatabaseTransactionLog;
        $transactionLog->setConnection($this->connection);

        print_r("DatabaseTransactionLogProvider class get function");die();
        return $transactionLog;
    }
}