<?php


namespace App\action;


class VldRequestData implements VldInterface /*VldRequestDataInterface*/
{
    public $user;

//    public $reqDataField =['full_name', 'current_state'];

    public function __construct(UserVldRolesInterface $user)
    {
        $this->user=$user;
    }

    public function get()
    {
        return ['full_name', 'current_state'];
    }

}
