<?php


namespace App\action;




use Ray\Di\AbstractModule;

class CheckValidate extends AbstractModule
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        // TODO: Implement configure() method.

        $this->bind(UserVldRolesInterface::class)
//            ->annotatedWith('role')
             ->to(UserVldRoles::class);
//
        $this->bind(VldRequestDataInterface::class)
//            ->annotatedWith('data')
             ->to(VldRequestData::class);



//        $this->bind(VldInterface::class)
//            ->toConstructor(
//                DataTesting::class,
//                [
//                    ['userRole' =>'roles'],
//                    ['reqDataField' => 'reqDataField']
//                ]
//            );
//
//        $this->bind()->annotated('roles')->toInstance('roles');
//        $this->bind()->annotated('reqDataField')->toInstance('reqDataField');
    }

}
