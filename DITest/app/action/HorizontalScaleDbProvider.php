<?php


namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';

use Ray\Di\AbstractModule;
use Ray\Di\Injector;
use Ray\Di\MethodInvocationProvider;
use Ray\Di\ProviderInterface;

class UserDb
{
    public $id;
    public function __construct($id)
    {
        $this->id = $id;
        //print_r($this->id);
    }
}
class HorizontalScaleDbProvider implements ProviderInterface
{
    private $invocationProvider;

    public function __construct(MethodInvocationProvider $invocationProvider)
    {
        $this->invocationProvider =$invocationProvider;
    }

    /**
     * @inheritDoc
     */
    public function get()
    {
        // TODO: Implement get() method.
        $methodInvocation = $this->invocationProvider->get();
        list($id) = $methodInvocation->getArguments()->getArrayCopy();
//        print_r($id);

        return new UserDb($id);
    }
}

class InjectorModule extends AbstractModule
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        // TODO: Implement configure() method.
        $this->bind(ProviderInterface::class)
            ->to(HorizontalScaleDbProvider::class);

//        $this->bind(LoggerInterface::class)
//            ->to(DatabaseLogger::class);
    }
}

$injector = new Injector(new InjectorModule);

$h = $injector->getInstance( HorizontalScaleDbProvider::class);
print_r($h->get());
