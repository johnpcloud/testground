<?php


namespace App\action;

use Ray\Di\AbstractModule;
use Ray\Di\Injector;

require __DIR__ . '../../../vendor/autoload.php';

interface ProcessorInterface
{

}
interface LoggerInterface
{

}

class PaypalProcessor implements ProcessorInterface
{

}
class DatabaseLogger implements LoggerInterface
{
    public $process;

    public function __construct(ProcessorInterface $process)
    {
        $this->process = $process;
    }

}




/*
 * Dependency Class
 */
class BillingModule extends AbstractModule
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        // TODO: Implement configure() method.
        $this->bind(ProcessorInterface::class)
            ->to(PaypalProcessor::class);
        $this->bind(LoggerInterface::class)
            ->to(DatabaseLogger::class);
    }
}



class BillingService
{
    private $processor;
//    private $logger;

    public function __construct(ProcessorInterface $processor/*, LoggerInterface $logger*/)
    {
        $this->processor = $processor;
//        $this->logger = $logger;
    }
}

class StoreService
{
//    private $processor;
    private $logger;

    public function __construct(/*ProcessorInterface $processor,*/ LoggerInterface $logger)
    {
//        $this->processor = $processor;
        $this->logger = $logger;
    }
}

$injector = new Injector(new BillingModule);
$billingService = $injector->getInstance(BillingService::class);
print_r($billingService);

$storeService = $injector->getInstance(StoreService::class);
//print_r($storeService);

