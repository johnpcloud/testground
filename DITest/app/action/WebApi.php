<?php


namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';

use PDO;
use Ray\Di\AbstractModule;
use Ray\Di\InjectionPoints;
use Ray\Di\Injector;
use Ray\Di\Di\Named;
use Ray\Di\Di\Inject;

// 	public PDO::__construct ( string $dsn [, string $username [, string $password [, array $options ]]] )



interface WebApiInterface
{

}
class Web implements WebApiInterface
{
    private $id;
    private $password;

    /**
     *  @Named("id=id,password=password")
     */
    public function __construct(string $id, string $password)
    {
        $this->id = $id;
        $this->password = $password;
    }
}


class PdoModule extends AbstractModule
{
    protected function configure()
    {
        $this->bind(Web::class)
//            ->toConstructor(Web::class, [['id'=>'id'], ['password'=>'password']]
            ->toConstructor(Web::class, 'id=id,password=password');

        $this->bind()->annotatedWith('id')->toInstance('123');
        $this->bind()->annotatedWith('password')->toInstance('pass-pass');
    }
}

$injector = new Injector(new PdoModule);
$pdo = $injector->getInstance(Web::class);
$works = $pdo instanceof Web;

print_r($pdo);die();

echo ($works ? 'It works!' : 'It DOES NOT work!') . PHP_EOL;
