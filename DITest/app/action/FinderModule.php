<?php

declare(strict_types=1);

namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';

use Ray\Di\AbstractModule;
use Ray\Di\Injector;
use Ray\Di\Di\Named;


interface DbInterface
{
}

class Db implements DbInterface
{
    private $dsn;
    private $username;
    private $password;

    /**
     * @Named("dsn,usernames,passwords")
     */

    public function __construct($dsn,$usernames,$passwords)
    {
        $this->dsn = $dsn;
        $this->username = $usernames;
        $this->password = $passwords;
    }

}

class PostDb implements DbInterface
{
    private $dsn;
    private $username;
    private $password;

    public function __construct($dsn,$username,$password)
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
    }

}

class FinderModule extends AbstractModule
{

//    private $className;
//    public function __construct($className,AbstractModule $module = null)
//    {
//        parent::__construct($module);
//        $this->className = $className;
////        print_r( gettype($this->className) );die();
//    }

    protected function configure()
    {
//        print_r( $this->className );
//        $this->bind(Db::class)->toConstructor(Db::class, 'dsn=dsn,username=username,password=password');

//        $this->bind($this->className)->toConstructor($this->className, ['dsn'=>'dsn','username'=>'username','password'=>'password']);


        $this->bind(Db::class)->toConstructor(Db::class, ['dsn'=>'dsn','usernames'=>'user','passwords'=>'pass']);
//        $this->bind(PostDb::class)->toConstructor(PostDb::class, ['dsn'=>'dsn','user'=>'username','password'=>'password']);

        $this->bind()->annotatedWith('dsn')->toInstance('msql:host=localhost;dbname=test');
        $this->bind()->annotatedWith('user')->toInstance('root');
        $this->bind()->annotatedWith('pass')->toInstance('52');

    }
}
$dbName = Db::class;

//$injector = new Injector(new FinderModule(new $dbName));


$injector = new Injector(new FinderModule);
$temp = $injector->getInstance($dbName);

//$temp = $injector->getInstance(Db::class);


print_r( $temp );
