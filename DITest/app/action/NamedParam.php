<?php


namespace App\action;


require __DIR__ . '../../../vendor/autoload.php';

use Ray\Di\AbstractModule;
use Ray\Di\Injector;
use Ray\Di\Di\Inject;
use Ray\Di\Di\Named;

interface FinderInterface
{
}

class LegacyFinder implements FinderInterface
{
    public $legacyfind;
    /**
     * @Named("legacy")
     * @Inject
     */
    public function legacyFind()
    {
        $data = 'legacyFind';
        return $this->legacyfind = 150;
//        return 'setFindFromLegacy';
    }

}

class ModernFinder implements FinderInterface
{
    public $modernfin;
    /**
     * @Named("finer")
     * @Inject
     */
    public function modernFind()
    {
        return $this->modernfin = 222;
    }

}

interface MovieListerInterface
{
}

class MovieLister implements MovieListerInterface
{
    public $finder;
    public $fin;



    /**
     * @Named("finder=legacy,fin=finer")
     */
    public function __construct(FinderInterface $finder,FinderInterface $fin)
    {
        $this->finder = $finder;
        $this->fin = $fin;
    }

}

class NamedParam extends AbstractModule
{
    protected function configure()
    {
        $this->bind(FinderInterface::class)->annotatedWith('legacy') ->to(LegacyFinder::class);
        $this->bind(FinderInterface::class)->annotatedWith('finer') ->to(ModernFinder::class);
        $this->bind(MovieListerInterface::class)->to(MovieLister::class);


    }
}

$injector = new Injector(new NamedParam);
$movieLister = $injector->getInstance(MovieListerInterface::class);
//$movieLister = $injector->getInstance(MovieLister::class);

print_r($movieLister);die();
