<?php


namespace App\action;

use Ray\Di\AbstractModule;
use Ray\Di\Injector;
use Ray\Di\Di\Named;

require __DIR__ . '../../../vendor/autoload.php';

class DataVld
{
    /**
     * @Named('showDataVldField')
    */
    public function getShowDataVldField()
    {
        return ['id'];
    }

    /**
     * @Named('createDataVldField')
     */
    public function getCreateDataVldField()
    {
        return ['title','description'];
    }
}

class RoleVld
{
    /**
     * @Named('showUserVldRole')
    */
    public function getShowVldRole()
    {
        return ['admin','user'];
    }

    /**
     * @Named('createUserVldRole')
     */
    public function getCreateVldRole()
    {
        return ['admin','user'];
    }
}

class ShowData
{
    private $reqData;
    private $userRole;
    private $popo;

    /**
     * @Named("reqData=showDataVldField,userRole=showUserVldRole")
     */
    public function __construct($reqData,$userRole)
    {
        $this->reqData = $reqData;
        $this->userRole = $userRole;
        $this->popo = 'some popo class';
    }
}

class CreateData
{
    private $reqData;
    private $userRole;
    private $popo;

    /**
     * @Named("reqData=createDataVldField,userRole=createUserVldRole")
     */
    public function __construct($reqData,$userRole)
    {
        $this->reqData = $reqData;
        $this->userRole = $userRole;
        $this->popo = 'some popo class';
    }
}
class DiManager extends AbstractModule
{
    protected function configure()
    {
        $this->bind(ShowData::class)
            ->toConstructor(ShowData::class,
                [
                    'reqData'=>'reqData',
                    'userRole'=>'userRole'
                ]
            );

        $this->bind()->annotatedWith('reqData')->toInstance(new DataVld);
        $this->bind()->annotatedWith('userRole')->toInstance(new RoleVld);

    }
}

class ConstructDiManager extends AbstractModule
{
    protected function configure()
    {
        /*$this->bind(ShowData::class)
            ->toConstructor(ShowData::class,
                [
                    'reqData'=>'reqData',
                    'userRole'=>'userRole'
                ]
            );*/

        $this->bind(ShowData::class)->annotatedWith('reqData')->toInstance(new DataVld);
        $this->bind(ShowData::class)->annotatedWith('userRole')->toInstance(new RoleVld);

    }
}


$className = ShowData::class;
$injector = new Injector(new DiManager);
$temp = $injector->getInstance($className);

print_r($temp);die();
