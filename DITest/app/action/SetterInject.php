<?php


namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';

use Ray\Di\AbstractModule;
use Ray\Di\Di\Inject;
use Ray\Di\Injector;

interface FinderInterface
{
}

class Finder implements FinderInterface
{
}

interface MovieListerInterface
{
}

class MovieLister implements MovieListerInterface
{
    public $finder;
    public $fin;

    /**
     * @Inject
     */
    public function setFinder(FinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @Inject
     */
    public function setFin(FinderInterface $fin)
    {
        $this->fin = $fin;
    }
}
class FinderModule extends AbstractModule
{
    protected function configure()
    {
        $this->bind(FinderInterface::class)->to(Finder::class);
        $this->bind(MovieListerInterface::class)->to(MovieLister::class);
    }
}



$injector = new Injector(new FinderModule);

//$movieLister = $injector->getInstance(FinderInterface::class);
$movieLister = $injector->getInstance(MovieListerInterface::class);

print_r($movieLister);die();
