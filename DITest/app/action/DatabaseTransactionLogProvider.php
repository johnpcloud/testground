<?php


namespace App\action;


use Ray\Di\ProviderInterface;

interface ConnectionInterface
{

}
class DatabaseTransactionLog implements ConnectionInterface
{
//    private $connection;
//
//    public function __construct(ConnectionInterface $connection)
//    {
//        $this->connection = $connection;
//    }
}
class DatabaseTransactionLogProvider implements ProviderInterface
{
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function get()
    {
        // TODO: Implement get() method.
        $transactionLog = new DatabaseTransactionLog;
        $transactionLog->setConnection($this->connection);

        return $transactionLog;
    }
}
