<?php


namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';


use Ray\Di\AbstractModule;
use Ray\Di\Injector;
use Ray\Di\ProviderInterface;

interface WebApiTInterface
{

}
class WebApiT implements WebApiTInterface
{
    private $id;
    private $password;
    /**
     * @Named("id=user_id,password=user_password")
     */
    public function __construct(string $id, string $password)
    {
        $this->id = $id;
        $this->password = $password;
    }
}
class WebUserApiT implements WebApiTInterface
{

}
class InjectorModule extends AbstractModule
{
    public $id = 1;
    public $pass = 'pass-pass';
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        // TODO: Implement configure() method.
        $this->bind(WebApiT::class)
            ->toConstructor(WebApiT::class,
                [
                    'id' => 'user_id',
                    'password' => 'user_pass'
                ]
            );

        $this->bind()->annotatedWith('user_id')->toInstance($this->id);
        $this->bind()->annotatedWith('password')->toInstance($this->pass);
    }
}

$injector = new Injector(new InjectorModule);
$newClass = $injector->getInstance(WebApiT::class);

print_r($newClass);
