<?php

declare(strict_types=1);
namespace App\action;

use League\Container\Container;

require __DIR__ . '../../../vendor/autoload.php';



class Foo
{
    /**
     * @var \App\action\BarInterface
     */
    public $bar;

    /**
     * Construct.
     *
     * @param \App\action\BarInterface $bar
     */
    public function __construct(BarInterface $bar)
    {
        $this->bar = $bar;
    }
}

interface BarInterface {}
class BarA implements BarInterface {}
class BarB implements BarInterface {}


/*
$container = new \League\Container\Container;

// Acme\Foo is added as normal but with an argument of Acme\BarInterface
$container->add(\App\action\Foo::class)->addArgument(\App\action\BarInterface::class);

// Acme\BarInterface is added as an alias with Acme\BarA as the concrete implementation,
// this could be swapped to Acme\BarB and that would be injected instead
//$container->add(\App\action\BarInterface::class, \App\action\BarA::class);
//$container->add(\App\action\BarInterface::class, \App\action\BarB::class);

$foo = $container->get(\App\action\Foo::class);
//print_r($foo);die();
*/

$container= new Container;
$container->add(Foo::class)->addArgument(BarInterface::class);
$container->add(BarInterface::class, BarA::class);
$container->add(BarInterface::class, BarB::class);
$foo = $container->get(Foo::class);
//$temp = $container->get(Foo::class);
//print_r($temp);



var_dump($foo instanceof \App\action\Foo);               // true
var_dump($foo->bar instanceof \App\action\BarInterface); // true
var_dump($foo->bar instanceof \App\action\BarA);         // true
var_dump($foo->bar instanceof \App\action\BarB);         // false
