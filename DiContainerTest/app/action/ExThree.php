<?php


namespace App\action;

require __DIR__ . '../../../vendor/autoload.php';

use League\Container\Container;

class Foo
{
    public $foo;
    public function setFoo()
    {

//        $data= ['id'];
        return $this->foo ;
    }
}
class Bar
{
    public $bar;
    public function setBar()
    {
//        $data = ['Title','description'];
        return $this->bar;
    }
}
class ExThree
{
    public $varFoo;
    public $varBar;
    /**
     * @param Foo $varBar
     * @parm Bar $varBar
    */
    public function __construct( $varFoo,  $varBar)
    {
        $this->varFoo = $varFoo;
        $this->varBar = $varBar;
    }
}

$container = new Container();

$container->add(ExThree::class)
    ->addArgument(Foo::class)
    ->addArgument(Bar::class)
    ->addMethodCall('setFoo',[Foo::class])
    ->addMethodCall('setBar',[Bar::class]);

$temp = $container->get(ExThree::class);
print_r($temp);
