<?php


namespace App\action;

use League\Container\Container;
use League\Container\Definition\Definition;
use League\Container\Definition\DefinitionAggregate;

require __DIR__ . '../../../vendor/autoload.php';

class Bar
{
    public function setBar()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'UserRole';
        return ['name','des'];
    }

    public function setFooBar()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'UserRole';
        return ['title','name','des'];
    }
}
class Baz
{
    public function setBaz()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'DataRole';
        return ['id'];
    }
    public function setFooBaz()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'DataRole';
        return ['id','name'];
    }
}

class Foo
{
    public function __construct( $bar,  $baz)
    {
        $this->bar = $bar;
        $this->baz = $baz;
    }
}

class FooBar
{
    public function __construct( $bar,  $baz)
    {
        $this->bar = $bar;
        $this->baz = $baz;
    }
}

$bar = 'setBar';
$baz = 'setBaz';

$className = FooBar::class;
$container = new Container;


    if($className === 'App\\action\\Foo')
        $container->add($className)->addArgument(Bar::setBar())->addArgument(Baz::setBaz());
    else
        $container->add($className)->addArgument(Bar::setFooBar())->addArgument(Baz::setFooBaz());


$temp = $container->get($className);

print_r($temp);

/*$container->add(Foo::class, function () {
    $pdo   = new Foo('setBar', 'setBaz');

    $modelBaz = new Baz();
    $modelBaz->setBaz($pdo);

    $modelBar = new Bar();
    $modelBar->setBar($pdo);

    return new Foo($modelBaz);
});*/

/*$definations = [
    (new Definition(Foo::class))->addArgument( Bar::setBar() )->addArgument(Baz::$baz())
];

$aggregate = new DefinitionAggregate($definations);
$container = new Container($aggregate);
*/


