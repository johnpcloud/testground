<?php


namespace App\action;

use League\Container\Container;

require __DIR__ . '../../../vendor/autoload.php';

class Controller
{
    /**
     * @var Model
     */
    public $model;

    /**
     * Construct.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}

class Model
{
    /**
     * @var \P
     */
    public $pdo;

    /**
     * Set PDO.
     *
     * @param \P $pdo
     */
    public function setPdo( $pdo)
    {
        $this->pdo = $pdo;
    }

    public function setSET()
    {
        return 'jhasdvgfjhvg';
    }
}

class P
{
    public $dsn_string;
    public $username;
    public $password;

    public function __construct($dsn_string,$username,$password)
    {
        $this->dsn_string = $dsn_string;
        $this->username = $username;
        $this->password = $password;
    }
}

$container = new Container;

//$container->add(Controller::class)->addArgument(Model::class);

$container->add(Controller::class, function () {
    $pdo   = new P('dsn+string', 'username', 'password');
    $model = new Model;

    $model->setPdo($pdo);

    return new Controller($model);
});

$controller = $container->get(Controller::class);

print_r($controller);die();

/*
var_dump($controller instanceof Acme\Controller);   // true
var_dump($controller->model instanceof Acme\Model); // true
var_dump($controller->model->pdo instanceof PDO);   // true
*/
