<?php


namespace App\action;

use League\Container\Container;

require __DIR__ . '../../../vendor/autoload.php';


class Bar
{
    public function setBar()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'UserRole';
        return ['name'];
    }

    public function setFooBar()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'UserRole';
        return ['title','name'];
    }
}
class Baz
{
    public function setBaz()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'DataRole';
        return ['id'];
    }
    public function setFooBaz()
    {
//        $className= 'MyClass';
//        return 'validate'.$className.'DataRole';
        return ['id','name'];
    }
}

class Foo
{
    public function __construct( $bar,  $baz)
    {
        $this->bar = $bar;
        $this->baz = $baz;
    }
}

class FooBar
{
    public function __construct( $bar,  $baz)
    {
        $this->bar = $bar;
        $this->baz = $baz;
    }
}

$container= new Container;
$container->add(Foo::class)
    ->addArgument(Bar::setBar())
    ->addArgument(Baz::setBaz())
    /*->addTag('fooClass')*/;
$container->add(FooBar::class)
    ->addArgument(Bar::setFooBar())
    ->addArgument(Baz::setFooBaz())
    /*->addTag('fooBarClass')*/;

$temp = $container->get(FooBar::class);
print_r($temp);
